1. Membuat Database
    create database formative4;
2. Membuat tabel student
    create table student(
    -> id int not null auto_increment,
    -> name varchar(30) not null,
    -> email varchar(30),
    -> phone varchar(15),
    -> birthdate date,
    -> primary key(id));
3. Membuat tabel course
     create table course(
    -> id int not null auto_increment,
    -> name varchar(30) not null,
    -> primary key(id));
4. Membuat tabel scoresheet
    create table scoresheet(
    -> id int not null,
    -> studentId int,
    -> courseId int,
    -> primary key(id),
    -> foreign key(studentId) references student(id),
    -> foreign key(courseId) references course(id));

    karena task 2 membutuhkan column score dalam table scoresheet, maka menambahkan column dengan perintah:

        alter table scoresheet add column score int;